# azure-deployment
Preparing an AKS Azure Kubernetes Service cluster for deploying the JCR (https://gitlab.com/jointcyberrange.nl/jcr-deploy-by-argocd and https://gitlab.com/jointcyberrange.nl/ctfd-jcr)

## Getting started with an account

login:
```bash
az login
```
and a web page will open, for logging in.

```bash
az account list  -o table
```
Continue with configuration.md
