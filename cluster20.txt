az aks create --resource-group JCR-staging --name jcr-argotest20-peter \
--enable-node-public-ip --generate-ssh-keys --nodepool-name argo20 --node-count 3 \
--nodepool-allowed-host-ports 80/tcp,443/tcp,53/tcp,53/udp,30000-60000/tcp,30000-50000/udp \
-l westeurope --enable-addons http_application_routing

# --node-vm-size -s Standard_DS2_v2